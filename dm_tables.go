package dameng

import (
	"context"
	"github.com/gogf/gf/v2/util/gutil"

	"github.com/gogf/gf/v2/database/gdb"
)

const (
	tablesSqlTpl = `SELECT TABLE_NAME FROM DBA_TABLES WHERE OWNER = ? UNION SELECT VIEW_NAME FROM DBA_VIEWS WHERE OWNER = ?`
)

// Tables retrieves and returns the tables of current schema.
// It's mainly used in cli tool chain for automatically generating the models.
func (d *Driver) Tables(ctx context.Context, schema ...string) (tables []string, err error) {
	var (
		result     gdb.Result
		usedSchema string
	)
	usedSchema = gutil.GetOrDefaultStr(getCurrentSchema(d), schema...)
	// When schema is empty, return the default link
	link, err := d.SlaveLink(schema...)
	if err != nil {
		return nil, err
	}
	// The link has been distinguished and no longer needs to judge the owner
	result, err = d.DoSelect(ctx, link, tablesSqlTpl, usedSchema, usedSchema)
	if err != nil {
		return
	}
	for _, m := range result {
		if v, ok := m["TABLE_NAME"]; ok {
			tables = append(tables, v.String())
		}
	}
	return
}
