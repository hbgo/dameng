package dameng_test

import (
	"context"
	"fmt"
	"time"

	"github.com/gogf/gf/v2/container/garray"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/gogf/gf/v2/test/gtest"
)

var (
	db        gdb.DB
	dblink    gdb.DB
	dbErr     gdb.DB
	ctx       context.Context
	TableSize = 10
)

var (
	TestDBHost  = g.Cfg().MustGet(ctx, "db.host").String()
	TestDBPort  = g.Cfg().MustGet(ctx, "db.port").String()
	TestDBUser  = g.Cfg().MustGet(ctx, "db.user").String()
	TestDBPass  = g.Cfg().MustGet(ctx, "db.pass").String()
	TestDBName  = g.Cfg().MustGet(ctx, "db.name").String()
	TestDBType  = "dm"
	TestDBExtra = "schema=ELEVATOR"
	TestCharset = "utf8"
)

type User struct {
	ID          int64     `orm:"id"`
	AccountName string    `orm:"account_name"`
	PwdReset    int64     `orm:"pwd_reset"`
	AttrIndex   int64     `orm:"attr_index"`
	Enabled     int64     `orm:"enabled"`
	Deleted     int64     `orm:"deleted"`
	CreatedBy   string    `orm:"created_by"`
	CreatedTime time.Time `orm:"created_time"`
	UpdatedBy   string    `orm:"updated_by"`
	UpdatedTime time.Time `orm:"updated_time"`
}

func init() {
	node := gdb.ConfigNode{
		Host:             TestDBHost,
		Port:             TestDBPort,
		User:             TestDBUser,
		Pass:             TestDBPass,
		Name:             TestDBName,
		Type:             TestDBType,
		Extra:            TestDBExtra,
		Role:             "master",
		Charset:          TestCharset,
		Weight:           1,
		MaxIdleConnCount: 10,
		MaxOpenConnCount: 10,
		CreatedAt:        "created_time",
		UpdatedAt:        "updated_time",
		Debug:            true,
	}

	nodeLink := gdb.ConfigNode{
		Type: TestDBType,
		Name: TestDBName,
		Link: fmt.Sprintf(
			func() string {
				if TestDBExtra == "" {
					return "dm:%s:%s@tcp(%s:%s)/%s%s"
				} else {
					return "dm:%s:%s@tcp(%s:%s)/%s?%s"
				}
			}(),
			TestDBUser, TestDBPass, TestDBHost, TestDBPort, TestDBName, TestDBExtra,
		),
	}

	nodeErr := gdb.ConfigNode{
		Host:    TestDBHost,
		Port:    TestDBPort,
		User:    TestDBUser,
		Pass:    "1234",
		Name:    TestDBName,
		Type:    TestDBType,
		Role:    "master",
		Charset: TestCharset,
		Weight:  1,
	}

	gdb.AddConfigNode(gdb.DefaultGroupName, node)
	if r, err := gdb.New(node); err != nil {
		gtest.Fatal(err)
	} else {
		db = r
	}

	gdb.AddConfigNode("dblink", nodeLink)
	if r, err := gdb.New(nodeLink); err != nil {
		gtest.Fatal(err)
	} else {
		dblink = r
	}

	gdb.AddConfigNode("dbErr", nodeErr)
	if r, err := gdb.New(nodeErr); err != nil {
		gtest.Fatal(err)
	} else {
		dbErr = r
	}

	ctx = context.Background()
}

func dropTable(table string) {
	if _, err := db.Exec(ctx, fmt.Sprintf("DROP TABLE IF EXISTS %s", table)); err != nil {
		gtest.Fatal(err)
	}
}

func createTable(table ...string) (name string) {
	if len(table) > 0 {
		name = table[0]
	} else {
		name = fmt.Sprintf("random_%d", gtime.Timestamp())
	}

	dropTable(name)

	sql, _ := gdb.FormatMultiLineSqlToSingle(
		fmt.Sprintf(`
CREATE TABLE "%s"(
	"ID" BIGINT NOT NULL,
	"ACCOUNT_NAME" VARCHAR(128) DEFAULT '' NOT NULL,
	"PWD_RESET" INT DEFAULT 0 NOT NULL,
	"ENABLED" INT DEFAULT 1 NOT NULL,
	"DELETED" INT DEFAULT 0 NOT NULL,
	"ATTR_INDEX" INT DEFAULT 0 ,
	"CREATED_BY" VARCHAR(32) DEFAULT '',
	"CREATED_TIME" DATETIME NOT NULL,
	"UPDATED_BY" VARCHAR(32) DEFAULT '',
	"UPDATED_TIME" DATETIME NOT NULL,
	PRIMARY KEY ("ID")
);
	`, name))
	if _, err := db.Exec(ctx, sql); err != nil {
		gtest.Fatal(err)
	}

	return
}

func createInitTable(table ...string) (name string) {
	name = createTable(table...)
	array := garray.New(true)
	for i := 1; i <= TableSize; i++ {
		array.Append(g.Map{
			"id":           i,
			"account_name": fmt.Sprintf(`name_%d`, i),
			"pwd_reset":    0,
			"attr_index":   i,
			"created_time": gtime.Now().String(),
			"updated_time": gtime.Now(),
		})
	}
	result, err := db.Insert(context.Background(), name, array.Slice())
	gtest.Assert(err, nil)

	n, e := result.RowsAffected()
	gtest.Assert(e, nil)
	gtest.Assert(n, TableSize)
	return
}

func createSchema(schema ...string) (name string) {
	if len(schema) > 0 {
		name = schema[0]
	} else {
		name = fmt.Sprintf("random_%d", gtime.Timestamp())
	}

	dropSchema(name)

	sql := fmt.Sprintf(`CREATE SCHEMA %s AUTHORIZATION "%s";`, name, TestDBUser)
	if _, err := db.Exec(ctx, sql); err != nil {
		gtest.Fatal(err)
	}

	return
}

func dropSchema(schema string) {
	if _, err := db.Exec(ctx, fmt.Sprintf("DROP SCHEMA IF EXISTS %s", schema)); err != nil {
		gtest.Fatal(err)
	}
}
