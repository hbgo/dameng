package dameng

import (
	"fmt"
	"github.com/gogf/gf/v2/database/gdb"
)

func (d *Driver) Schema(schema string) *gdb.Schema {
	odb, err := gdb.NewByGroup(d.GetGroup())
	if err != nil {
		panic(err)
	}
	configNode := odb.GetConfig()
	configNode.Extra = fmt.Sprintf("schema=%s", schema)

	db, err := gdb.New(*configNode)
	if err != nil {
		panic(err)
	}
	return &gdb.Schema{
		DB: db,
	}
}
